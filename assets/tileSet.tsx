<?xml version="1.0" encoding="UTF-8"?>
<tileset name="bob donut" tilewidth="70" tileheight="70" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="70" height="70" source="tileBlock.png"/>
 </tile>
 <tile id="1">
  <image width="70" height="70" source="tileEmpty.png"/>
 </tile>
</tileset>
