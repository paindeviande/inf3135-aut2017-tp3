# Travail pratique 3

## Description

Legend of the Beignet, Breath of the Bob, est un jeu de plateforme développé en C en s'inspirant fortement du jeu
[Maze](https://bitbucket.org/ablondin-projects/maze-sdl) par Alexandre Blondin Massé. Ce travail a été réalisé 
dans le cadre du cours INF3135, Construction et maintenance de logiciels à l'automne 2017 à l'Université du Québec 
à Montréal.

## Auteurs

- Louis-Philippe Geoffrion GEOL15129306
- Dominic Brodeur-Gendron BROD17129101

## Fonctionnement

Legend of the Beignet est un jeu de plateforme mettant en vedette Bob, notre affamé mangeur de beignets. Au menu d'ouverture,
l'utilisateur peut appuyer sur les touches 1, 2 ou 3 du clavier pour accéder respectivement au jeu, aux crédits et à la
fermeture du jeu. Au niveau du jeu, Bob doit se déplacer dans le niveau afin de satisfaire sa faim! Il doit manger les 5 beignets. Pour ce faire, il doit
s'approcher de ceux-ci et entrer en contact. Bob se déplace à l'aide des flèches gauche et droite du clavier. Il peut
également sauter à l'aide de la flèches haut. Une fois le jeu gagné, il est possible de retourner au menu principal à
partir des crédits à l'aide de la barre d'espacement.

## Plateformes supportées

- Ubuntu 16.04 LTS

## Dépendances

- [SDL2](https://www.libsdl.org/)
- [SDL_image 2.0](https://www.libsdl.org/projects/SDL_image/)
- [TMX](https://github.com/baylej/tmx)
- [LibXML2](http://www.xmlsoft.org/)
- [zlib](http://zlib.net/)
- [Cmake](https://cmake.org/)

## Compilation
Pour compiler l'application, il suffit d'exécuter les commandes suivants dans le dossier source:
```
cd src
make
```
Il ne suffira que de lancer la commande suivante pour lancer l'application par la suite (du dossier ```src```):
```
./tp3
```
## Références

1. [Maze](https://bitbucket.org/ablondin-projects/maze-sdl) d'[Alexandre Blondin Massé](http://www.lacim.uqam.ca/~blondin/home)
2. [Ressources graphiques](https://gitlab.com/ablondin/inf3135-aut2017-tp3-assets)
3. [Énoncé du TP](https://gitlab.com/ablondin/inf3135-aut2017-tp3-enonce)

## Division des tâches

- [X] Programmation du menu principal (Dominic)
- [X] Conception des plateformes (Dominic)
- [X] Conception de l'écran de fin de jeu (Dominic)
- [X] Gestion des collisions (Louis-Philippe)
- [X] Gestion des feuilles de sprites (Dominic)
- [X] Gestion du personnage & animations (Dominic)

  - [X] lorsque le personnage saute;
  - [X] lorsque le personnage atterrit;
  - [X] lorsque le personnage se déplace au sol;
  - [X] lorsque le personnage se déplace dans les airs;
  - [X] lorsque le personnage ne bouge pas;

- [X] Gestion des beignets (Dominic)
- [X] Implémentation de la gravité (Dominic)
- [X] Gestion GIT (Louis-Philippe)
- [X] Makefile et tests automatiques (Louis-Philippe)
- [X] README (Louis-Philippe)
- [X] Debuggage des coquilles majeures (Louis-Philippe & Dominic)
- [X] Refactorisation (Dominic)

## Statut

Le projet est complet et ne contient que de mineurs bugs de collision lorsque l'utilisateur saute vers le coin d'une 
plateforme ou lorsque Bob tombe en glissant sur un mur et en se déplaçant vers le dit mur simultanément.