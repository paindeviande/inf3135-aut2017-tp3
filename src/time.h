
#ifndef INF3135_TP3_TIME_H
#define INF3135_TP3_TIME_H

#include "sdl2.h"
#include <stdbool.h>

//Global variables so everything in the game can have access to those variables.
//This will enable everything to have the same delta time instead of saving the
// elapsed time for every script that needs it
float deltaTime;


/**
 * Ajust the time scale of the game,
 * this will also make the variable deltaTime to work properly.
 * @param timeScale
 */
void SetTimeScale(float timeScale);

/**
 * This will calculate the variable deltaTime,
 * call each update to have the time elapsed between each frame.
 * This makes the game indepedant of lag
 */
void UpdateTime();


#endif //INF3135_TP3_TIME_H
