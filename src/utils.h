#ifndef UTILS_H
#define UTILS_H

// -------------- //
// Data structure //
// -------------- //

enum Direction {     // Elementary directions
    DIRECTION_IDLE,
    DIRECTION_RIGHT, // Right
    DIRECTION_LEFT,  // Left direction
};

struct Point { // Representing a 2D point
    int x;     // The x-coordinate
    int y;     // The y-coordinate
};

struct Vector { // Representing a 2D floating point
    float x;     // The x-coordinate
    float y;     // The y-coordinate
};

struct Position { // Representing a position in the maze
    int row;      // The row number
    int col;      // The column number
};

#endif
