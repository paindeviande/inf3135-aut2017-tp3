#include "constants.h"
#include "objects.h"

struct Object *Object_create(enum ObjectType type, int positionX, int positionY, SDL_Renderer *renderer)
{
    struct Object *object;
    object = (struct Object*)malloc(sizeof(struct Object));

    object->type = type;
    object->renderer = renderer;
    object->position.x = positionX;
    object->position.y = positionY;
    object->enabled = true;

    if(type == DONUT)
    {
        object->animatedSpritesheet =
                AnimatedSpritesheet_create(DONUT_SPRITESHEET, 1, 32, 32,
                                           CHARACTER_BETWEEN_FRAME, renderer);

        object->animatedSpritesheet->spritesheet->scale = DONUT_SCALE;
    }

    return object;
}

void Object_delete(struct Object *object) {
    if (object != NULL) {
        AnimatedSpritesheet_delete(object->animatedSpritesheet);
        free(object);
    }
}

void Object_render(struct Object *object) {
    AnimatedSpritesheet_run(object->animatedSpritesheet);
    AnimatedSpritesheet_render(object->animatedSpritesheet,
                               (int)object->position.x,
                               (int)object->position.y);
}

bool Object_Collision(struct Vector position1, struct Vector position2)
{
    float min = 40;
    float diffx = abs(position1.x - position2.x);
    float diffy = abs(position1.y - position2.y);

    if(diffx + diffy < min)
        return true;
    return false;
}
