#include "time.h"
#include <stdio.h>
#include "sdl2.h"

unsigned int _lastUpdate = 0;
float _timeScale;


void SetTimeScale(float timeScale)
{
    _lastUpdate = SDL_GetTicks();
    _timeScale = timeScale;
}

void UpdateTime()
{
    unsigned int currentTime = SDL_GetTicks();
    if(currentTime > _lastUpdate)
    {
        deltaTime = (currentTime - _lastUpdate) * _timeScale / 100;
        _lastUpdate = currentTime;
    }
}

