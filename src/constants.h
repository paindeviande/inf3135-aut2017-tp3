// General constants
#define SCREEN_WIDTH  700
#define SCREEN_HEIGHT 700

// Menu constants
#define TITLE_FILENAME "../assets/title.jpg"
#define TITLE_WIDTH 700
#define TITLE_X ((SCREEN_WIDTH - TITLE_WIDTH) / 2)
#define TITLE_Y 0

#define PLAY_FILENAME "../assets/play.png"
#define PLAY_WIDTH 400
#define PLAY_X ((SCREEN_WIDTH - PLAY_WIDTH) / 1.5)
#define PLAY_Y 200

#define CREDIT_FILENAME "../assets/credit.png"
#define CREDIT_WIDTH 400
#define CREDIT_X ((SCREEN_WIDTH - QUIT_WIDTH) / 1.5)
#define CREDIT_Y 300

#define QUIT_FILENAME "../assets/quit.png"
#define QUIT_WIDTH 400
#define QUIT_X ((SCREEN_WIDTH - QUIT_WIDTH) / 1.5)
#define QUIT_Y 400

// Credit Menu constants
#define CREDIT_TITLE_FILENAME "../assets/creditTitle.jpg"
#define CREDIT_TITLE_WIDTH 1200
#define CREDIT_TITLE_X ((CREDIT_TITLE_WIDTH - CREDIT_TITLE_WIDTH) / 2)
#define CREDIT_TITLE_Y 0

// Stage
#define STAGE_FILENAME "../assets/stage2.tmx"
#define STAGE_OBJECT_FILENAME "../assets/stage1_objects.txt"
#define STAGE_INITIAL_X 0
#define STAGE_INITIAL_Y 0

// Character
#define CHARACTER_SPRITESHEET "../assets/character.png"
#define CHARACTER_SCALE (SCREEN_WIDTH / 2000.0)
#define CHARACTER_SIZE (128 * CHARACTER_SCALE)

#define CHARACTER_IDLE_ROW 2
#define CHARACTER_WALKING_LEFT_ROW 1
#define CHARACTER_WALKING_RIGHT_ROW 0

#define CHARACTER_VERTICAL_JUMP_ROW 3
#define CHARACTER_JUMP_LEFT_ROW 5
#define CHARACTER_JUMP_RIGHT_ROW 4

#define CHARACTER_BETWEEN_FRAME 25

#define CHARACTER_MOVE_SPEED 15
#define CHARACTER_JUMP_HEIGHT 65

#define CHARACTER_START_POSX 80
#define CHARACTER_START_POSY 150

// Donuts
#define DONUT_SPRITESHEET "../assets/donut.png"
#define DONUT_SCALE (SCREEN_WIDTH / 2000.0)

//OTHER
#define GRAVITY 9.81;