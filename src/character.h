#ifndef CHARACTER_H
#define CHARACTER_H

#include "utils.h"
//#include "maze.h"
#include "sdl2.h"
#include "animated_spritesheet.h"
#include "time.h"
#include "stage.h"

// --------------- //
// Data structures //
// --------------- //
enum CharacterState{
    BOB_IDLE,
    BOB_WALKING_RIGHT,
    BOB_WALKING_LEFT,
    BOB_JUMP_VERTICALLY,
    BOB_JUMP_RIGHT,
    BOB_JUMP_LEFT,
};


struct Character{
    enum CharacterState state;
    unsigned int frame;
    unsigned int height;
    unsigned int width;
    struct Vector position;
    struct Vector velocity;

    bool isGrounded;

    struct AnimatedSpritesheet *animatedSpritesheet; // His spritesheet
    SDL_Renderer *renderer;                          // The renderer
    enum Direction direction;
};

// --------- //
// Functions //
// --------- //

/**
 * Creates the character.
 *
 * @param renderer   The renderer
 * @return           A pointer to the character, NULL if there was an error;
 *                   Call IMG_GetError() for more information.
 */
struct Character *Character_create(SDL_Renderer *renderer);

/**
 * Deletes the character.
 *
 * @param character  The character to delete
 */
void Character_delete(struct Character *character);

/**
 * Renders the character.
 *
 * @param character  The character to render
 */
void Character_render(struct Character *character);


/**
 * Update the position of the character
 *
 * @param character    The character to update
 */
void Character_update(struct Character *character, struct Stage *stage);

/**
 * Moves the character.
 *
 * @param character  The character to move
 * @param direction  The direction to move
 */
void Character_move(struct Character *character, enum Direction direction);

/**
 * Make the character jump.
 *
 * If the character is already jumping, it has no effect.
 *
 * @param character  The character to make jump
 */
void Character_jump(struct Character *character);

/**
 * Check if the character is in collision with the stage
 *
 * @param character
 * @param stage
 * @param wallTopLeft
 * @param wallTopRight
 * @param wallBottomLeft
 * @param wallBottomRight
 * @param direction
 * @return          if there's a collision
 */
//bool Character_Collision(struct Character *character, struct Stage *stage, bool wallTopLeft, bool wallTopRight, bool wallBottomLeft, bool wallBottomRight, enum Direction direction);
bool Character_Collision(struct Character *character, struct Stage *stage,
                         float posYBottom, float testPosX, float testPosY,
                         bool wallTopLeft, bool wallTopRight, bool wallBottomLeft, bool wallBottomRight, enum Direction direction);

/**
 * Check if the character is on a structure
 *
 * @param character
 * @param stage
 * @param wallTopLeft
 * @param wallTopRight
 * @param wallBottomLeft
 * @param wallBottomRight
 * @param wallBottomMiddle
 * @return if the character is on a structure
 */
bool Character_OnStructure(struct Character *character, struct Stage *stage, bool wallTopLeft, bool wallTopRight, bool wallBottomLeft, bool wallBottomRight,bool wallBottomMiddle);

#endif