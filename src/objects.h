#ifndef INF3135_TP3_OBJECTS_H
#define INF3135_TP3_OBJECTS_H

#include "utils.h"
#include "sdl2.h"
#include "animated_spritesheet.h"
#include "time.h"

//Only one for now
enum ObjectType{
    DONUT = 0
};

struct Object
{
    enum ObjectType type;
    struct Vector position;
    bool enabled;
    struct AnimatedSpritesheet *animatedSpritesheet; // His spritesheet
    SDL_Renderer *renderer;
};

/**
 * Creates the object.
 *
 * @param type       The type of object
 * @param positionX  The x position
 * @param positionY  The y position
 * @param renderer   The renderer
 * @return           A pointer to the character, NULL if there was an error;
 *                   Call IMG_GetError() for more information.
 */
struct Object *Object_create(enum ObjectType type, int positionX, int positionY, SDL_Renderer *renderer);


/**
 * Deletes the object.
 *
 * @param object  The object to delete
 */
void Object_delete(struct Object *object);

/**
 * Renders the object.
 *
 * @param object  The object to render
 */
void Object_render(struct Object *object);

/**
 * Return if there's a collision with the 2 objects.
 * Every thing in this game is the same size, their size would be needed
 * if this game uses different sizes.
 *
 * @param position1     the position of the object 1
 * @param position2     the position of the object 2
 * @return              if there's a collision
 */
bool Object_Collision(struct Vector position1, struct Vector position2);

#endif //INF3135_TP3_OBJECTS_H
