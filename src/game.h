#ifndef GAME_H
#define GAME_H

#include "stage.h"
#include "character.h"
#include "objects.h"
#include "time.h"

// --------------- //
// Data structures //
// --------------- //

enum GameState {
    GAME_PLAY, // The player is playing
    GAME_MENU, // The player is returning to the menu
    GAME_CREDIT, // The player is in the credit menu
    GAME_QUIT  // The player is quitting
};

struct Game {
    struct Stage *stage;         // The current stage
    struct Character *character; // The character

    int score;                   // The player's score
    unsigned int nbObject;       // The number of objects
    struct Object **objects;     // The objects
    SDL_Renderer *renderer;      // The renderer
    enum GameState state;        // The state of the game
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new game.
 *
 * @param   The renderer for the game
 * @return  A pointer to the game, NULL if there was an error
 */
struct Game *Game_initialize(SDL_Renderer *renderer);

/**
 * Delete the game.
 *
 * @param game  The game to delete
 */
void Game_delete(struct Game *game);

/**
 * Start running the game.
 *
 * @param game  The game to run
 */
void Game_run(struct Game *game);

/**
 * Create an array of object from a file
 *
 * @param filename  the name of the file containing the objects info
 * @param game      the game
 */
void Game_loadObjects(char* filename, struct Game *game);

/**
 * Render every objects in the game
 *
 * @param game  the game struct
 */
void Game_RenderObjects(struct Game *game);

/**
 * Check if the character collide with an object and give points
 *
 * @param game   the game struct
 */
void Game_collisionCharacterObjects(struct Game *game);

/**
 * Run the credit menu if the player have all the donuts
 *
 * @param game  the game struct
 */
void Game_testVictory(struct Game *game);

#endif
