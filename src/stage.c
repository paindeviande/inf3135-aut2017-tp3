#include "stage.h"
#include <stdio.h>
/**
 * Generate the texture of the whole stage.
 *
 * @param stage  The stage whose texture is generated
 */
void Stage_generateTexture(struct Stage *stage);

static SDL_Renderer *Stage_renderer;

void* Stage_imageLoader(const char *path) {
    return IMG_LoadTexture(Stage_renderer, path);
}

// ---------------- //
// Public functions //
// ---------------- //

struct Stage *Stage_create(char *filename, SDL_Renderer *renderer) {
    // Warning: modifying global variables of TMX for loading map

    Stage_renderer = renderer;
    tmx_img_load_func = (void* (*)(const char*))Stage_imageLoader;
    tmx_img_free_func = (void  (*)(void*))      SDL_DestroyTexture;
    tmx_map *map = tmx_load(filename);


    if (!map) {
        return NULL;
    } else {
        struct Stage *stage;
        int i, j;
        stage = (struct Stage*)malloc(sizeof(struct Stage));
        stage->map = map;
        stage->renderer = renderer;
        Stage_generateTexture(stage);
        tmx_layer *layer = stage->map->ly_head;
        stage->collisionBox = (unsigned int*)calloc(stage->map->height * stage->map->width, sizeof(unsigned int));
        for (i = 0; i < stage->map->height; i++) {
            for (j = 0; j < stage->map->width; j++) {
                unsigned int gid;

                gid = layer->content.gids[(i * stage->map->width) + j];
                stage->collisionBox[i * stage->map->width + j] = stage->map->tiles[gid]->id;
            }
        }
        return stage;
    }
}


void Stage_render(struct Stage *stage) {
    SDL_RenderCopy(stage->renderer, stage->texture, NULL, NULL);
}

void Stage_delete(struct Stage *stage) {
    if (stage != NULL) {
        if (stage->texture != NULL) {
            SDL_DestroyTexture(stage->texture);
            stage->texture = NULL;
        }
        tmx_map_free(stage->map);
        free(stage->collisionBox);
        free(stage);
    }
}

// ----------------- //
// Private functions //
// ----------------- //

void Stage_generateTexture(struct Stage *stage) {
    tmx_map *map = stage->map;
    int textureWidth = map->width  * map->tile_width;
    int textureHeight = map->height * map->tile_height;
    stage->texture = SDL_CreateTexture(stage->renderer,
                                      SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                      textureWidth, textureHeight);
    SDL_SetRenderTarget(stage->renderer, stage->texture);
    SDL_RenderClear(stage->renderer);
    tmx_layer *layer;

    for (layer = map->ly_head; layer; layer = layer->next) {
        unsigned int i, j;
        for (i = 0; i < map->height; i++) {
            for (j = 0; j < map->width; j++) {

                unsigned int gid;
                SDL_Rect srcrect, dstrect;
                SDL_Texture* texture;

                gid = layer->content.gids[(i * map->width) + j];

                tmx_image *image = map->tiles[gid]->image;
                image = map->tiles[gid]->image;
                srcrect.x = map->tiles[gid]->ul_x;
                srcrect.y = map->tiles[gid]->ul_y;
                srcrect.w = image->width;
                srcrect.h = image->height;
                dstrect.w = image->width;

                dstrect.h = image->height;
                dstrect.x = j * image->width;
                dstrect.y = i * image->height;
                texture = (SDL_Texture*)image->resource_image;
                SDL_RenderCopy(stage->renderer, texture, &srcrect, &dstrect);
            }
        }
    }
    SDL_SetRenderTarget(stage->renderer, NULL);
}