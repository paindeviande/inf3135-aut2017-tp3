#include "game.h"
#include "sdl2.h"
#include "constants.h"
#include <stdio.h>
// --------------------------- //
// Private function prototypes //
// --------------------------- //

struct Game *Game_initialize(SDL_Renderer *renderer) {
    SetTimeScale(1);

    struct Game *game;
    game = (struct Game*)malloc(sizeof(struct Game));
    game->renderer = renderer;
    game->stage = Stage_create(STAGE_FILENAME, renderer);

    game->character = Character_create(renderer);
    game->state = GAME_PLAY;
    game->score = 0;
    Game_loadObjects(STAGE_OBJECT_FILENAME,game);

    return game;
}

void Game_delete(struct Game *game) {
    if (game != NULL) {
        Stage_delete(game->stage);
        Character_delete(game->character);
        free(game);
    }
}

void Game_run(struct Game *game) {

    SDL_Event e;
    game->character->position.x = CHARACTER_START_POSX;
    game->character->position.y = CHARACTER_START_POSY;
    game->character->direction = DIRECTION_IDLE;
    while (game->state == GAME_PLAY) {
        UpdateTime();
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                game->state = GAME_QUIT;
            } else if (e.type == SDL_KEYUP) {
                switch (e.key.keysym.sym)
                {
                    case SDLK_LEFT:
                        if (game->character->direction == DIRECTION_LEFT) {
                            Character_move(game->character, DIRECTION_IDLE);
                        }
                        break;
                    case SDLK_RIGHT:
                        if (game->character->direction == DIRECTION_RIGHT) {
                            Character_move(game->character, DIRECTION_IDLE);
                        }
                        break;
                }
            } else if (e.type == SDL_KEYDOWN) {
                enum Direction currentDir = game->character->direction;
                switch (e.key.keysym.sym)
                {
                    case SDLK_UP:
                        Character_jump(game->character);
                        break;
                    case SDLK_LEFT:
                        if (currentDir != DIRECTION_LEFT) {
                            Character_move(game->character, DIRECTION_LEFT);
                        }
                        break;
                    case SDLK_RIGHT:
                        if (currentDir != DIRECTION_RIGHT) {
                            Character_move(game->character, DIRECTION_RIGHT);
                        }
                        break;
                }
            }
            else
            {
                if(game->character->isGrounded)
                {
                    game->character->direction = DIRECTION_IDLE;
                    AnimatedSpritesheet_setRow(game->character->animatedSpritesheet,
                                           CHARACTER_IDLE_ROW);
                }
            }
        }
        
        Character_update(game->character, game->stage);
        SDL_SetRenderDrawColor(game->renderer, 0x50, 0x50, 0x00, 0x00 );
        SDL_RenderClear(game->renderer);

        Game_collisionCharacterObjects(game);
        Stage_render(game->stage);
        Character_render(game->character);
        Game_RenderObjects(game);
        SDL_RenderPresent(game->renderer);
    }
}

void Game_loadObjects(char* filename, struct Game *game)
{
    unsigned int nbItem; //from file

    FILE *fp;
    fp = fopen(filename,"r");
    if(fp == NULL)
    {
        printf("ERROR WHILE OPENING %s", filename);
        exit(1);
    }

    fscanf(fp,"%d",&nbItem);

    struct Object** objects = malloc(nbItem * sizeof(void*));
    game->objects = objects;
    game->nbObject = nbItem;

    int i;
    for(i = 0; i < nbItem; ++i)
    {
        int type, positionX, positionY;
        fscanf(fp,"%d %d %d", &type, &positionX, &positionY);
        objects[i] = Object_create(type,positionX,positionY,game->renderer);
    }
    fclose(fp);
}

void Game_collisionCharacterObjects(struct Game *game)
{
    int i;
    for(i = 0; i < game->nbObject; i++)
    {
        if(game->objects[i]->enabled)
        {
            if(Object_Collision(game->character->position,game->objects[i]->position))
            {
                game->score++;
                game->objects[i]->enabled = false;
                Game_testVictory(game);
            }
        }
    }
}

void Game_RenderObjects(struct Game *game)
{
    int i;
    for(i = 0; i < game->nbObject; i++)
    {
        if(game->objects[i]->enabled)
        {
            Object_render(game->objects[i]);
        }
    }
}

void Game_testVictory(struct Game *game)
{

    if(game->score == 5)
    {
        game->state = GAME_CREDIT;
    }
}