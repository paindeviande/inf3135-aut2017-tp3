#include "constants.h"
#include "character.h"

struct Character *Character_create(SDL_Renderer *renderer) {
    struct Character *character;
    character = (struct Character*)malloc(sizeof(struct Character));

    character->renderer = renderer;
    character->width = CHARACTER_SIZE;
    character->height = CHARACTER_SIZE;
    character->position.x = CHARACTER_START_POSX;
    character->position.y = CHARACTER_START_POSY;
    character->velocity.x = 0;
    character->velocity.y = 0;

    character->animatedSpritesheet =
            AnimatedSpritesheet_create(CHARACTER_SPRITESHEET, 6, 32, 192,
                                       CHARACTER_BETWEEN_FRAME, renderer);

    character->animatedSpritesheet->spritesheet->scale = CHARACTER_SCALE;
    return character;
}

void Character_delete(struct Character *character) {
    if (character != NULL) {
        AnimatedSpritesheet_delete(character->animatedSpritesheet);
        free(character);
    }
}

void Character_render(struct Character *character) {
    AnimatedSpritesheet_run(character->animatedSpritesheet);
    AnimatedSpritesheet_render(character->animatedSpritesheet,
                               (int)character->position.x,
                               (int)character->position.y);
}

void Character_move(struct Character *character,enum Direction direction)
{
    character->direction = direction;

    if(character->isGrounded)
    {
        switch (direction)
        {
            case DIRECTION_LEFT:
                AnimatedSpritesheet_setRow(character->animatedSpritesheet,
                                           CHARACTER_WALKING_LEFT_ROW);
                break;
            case DIRECTION_RIGHT:
                AnimatedSpritesheet_setRow(character->animatedSpritesheet,
                                           CHARACTER_WALKING_RIGHT_ROW);
                break;
            case DIRECTION_IDLE:
                AnimatedSpritesheet_setRow(character->animatedSpritesheet,
                                           CHARACTER_IDLE_ROW);
                break;
        }
    }

}

void Character_update(struct Character *character, struct Stage *stage) {

    float testDirection = 1;
    if (character->direction == DIRECTION_LEFT)
        testDirection = -1;

    //Calculate position where character should be after movement
    float testPosX = character->position.x + testDirection * CHARACTER_MOVE_SPEED * deltaTime;
    float testPosY = character->position.y + character->velocity.y * deltaTime;


    int posX, posY, posXRight, posYBottom, posXMiddle;
    posX = (int)(testPosX / stage->map->tile_width);
    posXRight = (int)((testPosX + character->width) / stage->map->tile_width);
    posY = (int)(testPosY / stage->map->tile_height);
    posYBottom = (int)((testPosY + character->height) / stage->map->tile_height);
    posXMiddle = (int)((testPosX + character->width/2) / stage->map->tile_width);

    bool wallTopLeft = (stage->collisionBox[stage->map->width * posY + posX] == 0);
    bool wallTopRight = (stage->collisionBox[stage->map->width * posY + posXRight] == 0);
    bool wallBottomLeft = (stage->collisionBox[stage->map->width * posYBottom + posX] == 0);
    bool wallBottomRight = (stage->collisionBox[stage->map->width * posYBottom + posXRight] == 0);
    bool wallBottomMiddle = (stage->collisionBox[stage->map->width * posYBottom + posXMiddle] == 0);

    if (character->direction != DIRECTION_IDLE)
    {

        switch (character->direction) {
            case DIRECTION_RIGHT:
                if(Character_Collision(character, stage, posYBottom, testPosX, testPosY,
                                       wallTopLeft, wallTopRight, wallBottomLeft, wallBottomRight, DIRECTION_RIGHT))
                    character->position.x = testPosX;
                break;

            case DIRECTION_LEFT:
                if(Character_Collision(character, stage, posYBottom, testPosX, testPosY,
                                       wallTopLeft, wallTopRight, wallBottomLeft, wallBottomRight, DIRECTION_LEFT))
                    character->position.x = testPosX;
                break;
        }
    }

    //Update gravity velocity
    if(!character->isGrounded)
    {
        character->velocity.y += deltaTime * GRAVITY;
    }
    if(Character_OnStructure(character, stage, wallTopLeft, wallTopRight, wallBottomLeft, wallBottomRight,wallBottomMiddle))
    {
        if(!character->isGrounded)
        {
            character->velocity.y = 0;
            character->isGrounded = true;
            Character_move(character,character->direction);
        }
    }
    else
    {
        character->position.y += character->velocity.y * deltaTime;
        character->isGrounded = false;
    }

    if(wallTopLeft && wallTopRight)
    {
        character->velocity.y = 0;
        character->position.y += character->velocity.y * deltaTime;
    }

    //Block position if out of bound
    if(character->position.x > SCREEN_WIDTH - character->width)
        character->position.x = SCREEN_WIDTH - character->width;
    if(character->position.x < 0)
        character->position.x = 0;
    if(character->position.y > SCREEN_HEIGHT - character->height)
    {
        character->position.y = SCREEN_HEIGHT - character->height;
        character->isGrounded = true;
        Character_move(character,character->direction);
    }
    if(character->position.y < 0)
    {
        character->position.y = 0;
        character->velocity.y = 0;
    }
}

void Character_jump(struct Character *character)
{
    if (character->isGrounded)
    {
        character->isGrounded = false;
        character->velocity.y -= CHARACTER_JUMP_HEIGHT;

        switch (character->direction)
        {
            case DIRECTION_RIGHT:
                AnimatedSpritesheet_setRow(character->animatedSpritesheet,
                                           CHARACTER_JUMP_RIGHT_ROW);
                break;
            case DIRECTION_LEFT:
                AnimatedSpritesheet_setRow(character->animatedSpritesheet,
                                           CHARACTER_JUMP_LEFT_ROW);
                break;
            case DIRECTION_IDLE:
                AnimatedSpritesheet_setRow(character->animatedSpritesheet,
                                           CHARACTER_VERTICAL_JUMP_ROW);
                break;
        }
    }

}

bool Character_Collision(struct Character *character, struct Stage *stage, float posYBottom, float testPosX, float testPosY,
                         bool wallTopLeft, bool wallTopRight, bool wallBottomLeft, bool wallBottomRight, enum Direction direction)
{
    bool validMove = true;
    float offSet = 0.075;

    if(direction == DIRECTION_LEFT)
    {
        if(wallTopLeft)
            validMove = false;

        if(!character->isGrounded && (wallBottomLeft || wallTopLeft))
        {
            validMove = false;
        }
        if(wallBottomLeft && ((testPosY+character->height)/stage->map->tile_height - posYBottom > offSet))
        {
            validMove = false;
        }
        else if(wallBottomLeft && ((testPosY+character->height)/stage->map->tile_height - posYBottom <= offSet))
        {
            character->position.y = posYBottom * stage->map->tile_height - character->height;
        }
    }
    else if(direction == DIRECTION_RIGHT)
    {
        if(wallTopRight)
            validMove = false;

        if(!character->isGrounded && (wallBottomRight || wallTopRight))
            validMove = false;
        if(wallBottomRight && ((testPosY+character->height)/stage->map->tile_height - posYBottom > offSet))
        {
            validMove = false;
        }
        else if(wallBottomRight && ((testPosY+character->height)/stage->map->tile_height - posYBottom <= offSet))
        {
            character->position.y = posYBottom * stage->map->tile_height - character->height;
        }
    }
    return validMove;
}

bool Character_OnStructure(struct Character *character, struct Stage *stage, bool wallTopLeft, bool wallTopRight, bool wallBottomLeft, bool wallBottomRight,bool wallBottomMiddle)
{
    bool onStructure = false;

    if((wallBottomLeft && wallBottomMiddle) || (wallBottomMiddle && wallBottomRight))
        onStructure = true;
    else if(character->direction == DIRECTION_LEFT && wallBottomLeft)
    {
        if(wallTopLeft && (wallBottomMiddle || wallBottomRight))
            onStructure = true;
    }
    else if(character->direction == DIRECTION_RIGHT && wallBottomRight)
    {
        if(wallTopRight && (wallBottomMiddle || wallBottomLeft))
            onStructure = true;
    }

    return onStructure;
}