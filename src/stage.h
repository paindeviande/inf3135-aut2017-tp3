#ifndef STAGE_H
#define STAGE_H

#include "utils.h"
#include "sdl2.h"
#include <stdbool.h>
#include <tmx.h>

// --------------- //
// Data structures //
// --------------- //

struct Stage {
    tmx_map *map;           // The TMX map
    struct Position start;  // The starting position
    struct Position end;    // The end position
    unsigned int *collisionBox;   // The collision map
    SDL_Texture *texture;   // The texture (image)
    SDL_Renderer *renderer; // The renderer
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a stage from a TMX file.
 *
 * @param filename  The path of the TMX file
 * @param renderer  The renderer
 * @return          A pointer to the create stage, NULL if there was an error;
 *                  Call IMG_GetError() for more information.
 */
struct Stage *Stage_create(char *filename, SDL_Renderer *renderer);

/**
 * Delete the given stage.
 *
 * @param stage  The stage to be deleted
 */
void Stage_delete(struct Stage *stage);

/**
 * Renders the stage with a renderer.
 *
 * @param stage      The stage to be rendered
 */
void Stage_render(struct Stage *stage);


#endif